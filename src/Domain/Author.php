<?php

namespace MyBooks\Domain;

/**
 * Author.php - Class representing an author.
 *
 * @author      Luciano Goulart
 * @version     1.0.0
 * @copyright   Professeur Baptiste Pesquet - OpenClassrooms
 */
class Author {

    /**
     * Author id.
     *
     * @var int
     */
    private $id;

    /**
     * Author firstname.
     *
     * @var string
     */
    private $firstname;

    /**
     * Author lastname.
     *
     * @var string
     */
    private $lastname;


    // GETTERS
    /**
     * Returns author id.
     *
     * @return string the author id.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns author firstname
     *
     * @return string the author firstname.
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Returns author lastname.
     *
     * @return string the author lastname.
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    // SETTERS
    /**
     * Sets author id.
     *
     * @param int $id
     * @return void
     */
    public function setid($id)
    {
        $this->id = $id;
    }

    /**
     * Sets author firstname.
     *
     * @param string $firstname
     * @return void
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * Sets author lastname.
     *
     * @param string $lastname
     * @return void
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }
}
