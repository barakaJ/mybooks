<?php

namespace MyBooks\DAO;

use MyBooks\Domain\Author;

/**
 * AuthorDAO.php - Access to author's data.
 *
 * @author      Luciano Goulart
 * @version     1.0.0
 * @copyright   Professeur Baptiste Pesquet - OpenClassrooms
 */
class AuthorDAO extends DAO {

    /**
     * Returns an author based on param id.
     *
     * @param int $id the author id.
     * @return object \MyBooks\Domain\Author the author.
     */
    public function find($id)
    {
        $sql = "SELECT * FROM author WHERE auth_id=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        return $this->buildDomainObject($row);
    }

    /**
     * Builds a Author object based on a database row.
     *
     * @param array $row The database row containing author data.
     * @return \MyBooks\Domain\Author
     */
    protected function buildDomainObject($row)
    {
        $author = new Author();
        $author->setId($row['auth_id']);
        $author->setFirstname($row['auth_first_name']);
        $author->setLastname($row['auth_last_name']);

        return $author;
    }

}
