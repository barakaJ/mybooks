## Université OpenClassrooms

**Formation :** Développeur d'application - PHP/Symfony.

**Cours :** Évoluez vers une architecture PHP professionnelle.

**Dirigé par :** Professeur Baptiste Pesquet.

**Exercice :** MyBooks - a simple book store developped in PHP and Silex to demonstrate PHP POO concepts.

**URL du cours :** https://openclassrooms.com/fr/courses/2560666-evoluez-vers-une-architecture-php-professionnelle-avec-silex

**Travail présenté par :** Luciano Goulart [lgoulart@lnx-it.inf.br].
