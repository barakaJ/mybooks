<?php
/**
 * prod.php - Database connection using Doctrine/DBAL.
 *
 * @author      Luciano Goulart
 * @version     1.0.0
 * @copyright   Professeur Baptiste Pesquet - OpenClassrooms
 */
$app['db.options'] = array(
    'driver'    => 'pdo_mysql',
    'charset'   => 'utf8',
    'host'      => 'localhost',
    'port'      => '3306',
    'dbname'    => 'mybooks',
    'user'      => 'mybooks_user',
    'password'  => 'secret',
);
