<?php
/**
 * dev.php - Configuration file for the development phase
 *
 * @author      Luciano Goulart
 * @version     1.0.0
 * @copyright   Professeur Baptiste Pesquet - OpenClassrooms
 */
// Production settings include.
// When this required is in use ou need to comment the doctrine block below.
require __DIR__ . '/prod.php';

// Doctrine
// $app['db.options'] = array(
//     'driver'    => 'pdo_mysql',
//     'charset'   => 'utf8',
//     'host'      => 'localhost',
//     'port'      => '3306',
//     'dbname'    => 'mybooks',
//     'user'      => 'mybooks_user',
//     'password'  => 'secret',
// );

// Enable the debug mode
//$app['debug'] = true;

// Define the log level
//$app['monolog.level'] = 'INFO';
