<?php

use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;

/**
 * app.php - Silex application configuration file.
 *
 * @author      Luciano Goulart
 * @version     1.0.0
 * @copyright   Professeur Baptiste Pesquet - OpenClassrooms
 */

// Register global error and exception handlers.
ErrorHandler::register();
ExceptionHandler::register();

//
// Register service providers.
//

// Register the service provider associated with Doctrine DBA.
$app->register(new Silex\Provider\DoctrineServiceProvider());

// Register the path of templates folder on Twig.
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../views',
));

// Register the service provider associated with twig-bridge component.
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// Register AuthorDAO.
$app['dao.author'] = $app->share(function ($app)
{
    return new MyBooks\DAO\AuthorDAO($app['db']);
});

// Register BookDAO.
$app['dao.book'] = $app->share(function ($app) {
    $bookDAO = new MyBooks\DAO\BookDAO($app['db']);
    $bookDAO->setAuthorDAO($app['dao.author']);

    return $bookDAO;
});
